import './App.css';
import AppRoutes from './AppRoutes';
import './pages/Dashboard';


function App() {
  return (
    <AppRoutes />
  );
}

export default App;
