import { Outlet } from "react-router-dom";
import imgBG from "./assets/imgs/bg.png";
import imgLogo from "./assets/imgs/logo.png";

import styled from "styled-components";

const Header = styled.div`
display: flex;
height: 100px;
width: 100%;
align-items: center;
justify-content:center;
`
const Container = styled.div`
background-image: url(${imgBG});
background-size: cover;
width: 100vw;
height: 100vh;
display: flex;
flex-direction: column;
align-items: center;
`

const Wrapper = styled.div`
background-color: #FFF;
width: 50vw;
height: 100%;
display: flex;
flex-direction: column;
align-items: center;
`


export default function TemplateForm(){
    return (
        <>
            <Container>
                <Wrapper>
                    <Header>
                        <img src={imgLogo} alt="Logomarca Voll"/>
                    </Header>

                    <Outlet />
                </Wrapper>
            </Container>
        </>
    );
}