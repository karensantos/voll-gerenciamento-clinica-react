import { action, makeObservable, observable } from "mobx";

interface IUser {
    email: string,
    token: string
}

class AuthStore {
    authenticated = false;
    user: IUser = {email: "", token: ""}

    constructor() {
        makeObservable(this, {
            authenticated: observable,
            user: observable, 
            login: action,
            logout: action
        })
    }

    login({email, token}: IUser){
        this.authenticated = true
        this.user = {email, token}
    }

    logout(){
        this.authenticated = false;
        this.user = {email: "", token: ""}
    }
}

const authStore = new AuthStore();

export default authStore;