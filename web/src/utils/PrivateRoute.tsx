import { Navigate, Outlet } from "react-router-dom";
import authStore from "../stores/auth.store"

const PrivateRoute = () => {
    const {authenticated} = authStore;

    return (
        authenticated ?  <Outlet/> : <Navigate to="/login" />
    )
}

export default PrivateRoute;