import styled from "styled-components";
import "./assets/style.css";


const Container = styled.div`
display: flex;
margin: auto;
width: 90%;
margin-top: 32px;
margin-bottom: 32px;
justify-content: center;
text-align: center;
`

const Wrapper = styled.div`
box-shadow: 4px 4px 8px 0px #00000026;
width: 99%;
height: 214px;
padding: 32px;
border-radius: 8px;
gap: 32px;
`
const InputWrapper = styled.div`
display: flex;
justify-content: center;
align-items: center;
gap: 16px;
color: #5C5C5C;
width: 85%;
margin: auto;
`

const InputGroup = styled.div`
display: flex;
box-shadow: 2px 2px 6px 0px #00000040;
border-radius: 8px;
width: 100%;
`

const CategoryWrapper = styled.div`
display: flex;
justify-content: center;
align-items: center;
gap: 16px;
`
const CategoryItem = styled.div`
display: flex;
background-color: #E7EBEF;
padding: 4px 8px;
border-radius: 4px;
color: #6B6E71;
gap: 10px;
cursor: pointer;
font-size: 14px;
`

function Searchbar() {
    return(
        <>
            <Container>
                <Wrapper>
                    <InputWrapper  className="input-wrapper">
                        <InputGroup className="input-group">
                            <i className="fa-solid fa-magnifying-glass"></i>
                            <input type="text" placeholder="Digite a especialidade"/>
                        </InputGroup>

                        <InputGroup className="input-group">
                            <i className="fa-sharp fa-solid fa-location-dot"></i>
                            <input type="text" placeholder="Digite a sua localização"/>
                        </InputGroup>

                        <button type="submit">Buscar</button>
                    </InputWrapper >

                    <h4>Você pode estar procurando por estas categorias:</h4>
                    <CategoryWrapper>
                        <CategoryItem className="category-item">Neurologista <span>x</span></CategoryItem>
                        <CategoryItem className="category-item">Dermatologista <span>x</span></CategoryItem>
                        <CategoryItem className="category-item">Cardiologista <span>x</span></CategoryItem>
                        <CategoryItem className="category-item">Ortopedita <span>x</span></CategoryItem>
                        <CategoryItem className="category-item">Oftalmologista <span>x</span></CategoryItem>
                        <CategoryItem className="category-item">Pediatria <span>x</span></CategoryItem>
                        <CategoryItem className="category-item">Reumatologista <span>x</span></CategoryItem>
                    </CategoryWrapper>
                </Wrapper>
            </Container>
        </>
    )
}

export default Searchbar;