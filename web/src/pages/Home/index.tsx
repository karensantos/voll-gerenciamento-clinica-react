import Actions from './Actions';
import Banner from './Banner';
import Searchbar from './Searchbar';
import Depositions from './Depositions';

export default function Home() {

    return(
        <>
            <Banner/>
            <Searchbar />
            <Actions />
            <Depositions />
        </>
    )
}