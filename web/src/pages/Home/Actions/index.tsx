import styled from "styled-components";
import calendar from "./assets/imgs/calendar_month.png";
import health from "./assets/imgs/health_and_safety.png";
import notification from "./assets/imgs/notifications.png";
import thumb from "./assets/imgs/thumb_up.png";

import './assets/style.css';


const Container = styled.div`
display: flex;
margin: auto;
width: 90%;
`

function Actions() {
    return(
        <>
            <Container className="cta-actions">
                <div className="action">
                    <img src={health} alt="" className="action-icon"/>
                    <p className="action-title">
                        Encontre especialistas
                    </p>
                </div>

                <div className="action">
                    <img src={calendar} alt="" className="action-icon"/>
                    <p className="action-title">
                        Agende consultas
                    </p>
                </div>

                <div className="action">
                    <img src={notification} alt="" className="action-icon"/>
                    <p className="action-title">
                        Defina lembretes
                    </p>
                </div>

                <div className="action">
                    <img src={thumb} alt="" className="action-icon"/>
                    <p className="action-title">
                        Avalie o serviço
                    </p>
                </div>
            </Container>
        </>
    )
}

export default Actions;