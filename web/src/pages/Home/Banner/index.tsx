import styled from "styled-components";
import banner from './assets/img/banner.png';

const WrapperBanner = styled.section`
height: 350px;
width: 100%;
background-image: url(${banner});
background-color: #339CFF;
background-size: contain;
background-position: bottom right;
display: flex;
flex-direction: column;
align-items: flex-start;
justify-content: center;
background-repeat: no-repeat;
`
const Title = styled.h1`
color: #fff;
max-width: 50%;
font-size: 2rem;
`
const Container = styled.div`
display: flex;
margin: auto;
width: 90%;
`

function Banner() {
    return (
        <WrapperBanner>
           <Container>
               <Title>Encontre profissionais de diversas especialidades e agende sua consulta com facilidade!</Title>
            </Container>
        </WrapperBanner>
    )
}

export default Banner;