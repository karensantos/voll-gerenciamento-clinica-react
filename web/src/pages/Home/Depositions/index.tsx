import styled from "styled-components";

const Container = styled.div`
display: flex;
flex-direction: column;
margin: auto;
width: 70%;
margin-top: 32px;
`

const Title = styled.h1`
color: #0B3B60;
font-weight: 700;
font-size: 24px;
`
const Header = styled.div`
text-align: center;
margin: auto;
`

const Item = styled.div`
color: #90989F;
border-bottom: 1px solid #E7EBEF;
gap: 16px;
margin-bottom: 32px;
`
const Label = styled.p`
color: #6B6E71;
font-weight: 700;
font-size: 16px;
text-align: center;
`

function Depositions() {
    return (
        <>
            <Container className="depositions">
                <Header className="title-section">
                    <Title>Depoimentos</Title>
                </Header>
                <div className="depositions-wrapper">               
                    <Item className="testimony">
                        <p className="testimony-text">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quNam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.
                        </p>
                        <Label className="testimony-person">Júlio, 40 anos, São Paulo/SP.
                        </Label>
                    </Item>

                    <Item className="testimony">
                        <p className="testimony-text">Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.</p>

                        <Label className="testimony-person">Júlio, 40 anos, São Paulo/SP.
                        </Label>
                    </Item>

                    <Item className="testimony">
                        <p className="testimony-text">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores.</p>

                        <Label className="testimony-person">Júlio, 40 anos, São Paulo/SP.
                        </Label>
                    </Item>
                </div>
            </Container>
        </>
    )
}

export default Depositions