import { Box, Modal, Switch, Checkbox,FormControlLabel, FormGroup } from "@mui/material";
import Titulo from "../../../components/Titulo";
import Input from "../../../components/Input";
import styled from "styled-components";
import Button from "../../../components/Botao";
import { useState } from "react";
import usePost from "../../../usePost";
import authStore from "../../../stores/auth.store";
import IProfissional from "../../../types/IProfissional";


const CustomBox = styled(Box)`
    position: fixed;
    top: 50%;
    left:50%;
    transform: translate(-50%, -50%);
    min-width: 300px;
    width: 30vw;
    max-height: 90vh;
    overflow-y: auto;
    background-color: #FFF;
    border: none;
    border-radius: 16px;
    padding: 1rem 5rem;
`;

const ContainerGrid = styled.div`
    display: grid;
    width: 100%;
    grid-template-columns: 50% 45%;
    justify-content: space-between;
`;

const ContainerGridCenter = styled(ContainerGrid)`
    grid-template-columns: auto;
    justify-content: center;
    text-align: center;
`

const Text = styled.p`
    font-weight: 400;
    font-size: 16px;
    line-height: 19px;
    color: var(--azul-escuro);
    text-align: left;
    width: 100%;
    font-weight: 700;
`;

const TextSwitch = styled(Text)`
    color: var(--cinza);
    text-align: center;
    margin: 0;
    font-size: 14px;
`

const SwitchPlan = styled(Switch)`
    margin:auto;
`

const ButtonCustom = styled(Button)`
width: 50%;
`;

export default function ModalRegister({open, handleClose } : {open: boolean, handleClose:()=> void}) {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [validatedPassword, setValidatedPassword] = useState('');
    const [CRM, setCRM] = useState('');
    const [specialty, setSpecialty] = useState('')
    const [phone, setPhone] = useState('');
    const [urlImage, setUrlImage] = useState('');
    const [code, setCode] = useState('');
    const [street, setStreet] = useState('');
    const [number, setNumber] = useState('');
    const [complement, setComplement] = useState('');
    const [state, setState] = useState('');
    const [acceptPlan, setAcceptPlan] = useState(false);
    const [selectPlans, setSelectPlans] = useState<number[]>([]);

    const { createData } = usePost();
    const {user} = authStore;

    const handleChangePlans = (event: React.ChangeEvent<HTMLInputElement>) => {
        const plan = parseInt(event.target.value);
        if(acceptPlan) {
            setSelectPlans([...selectPlans, plan])
        } else {
            setSelectPlans([...selectPlans.filter(p => p !== plan)])
        }
    }

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const profissional: IProfissional = {
            nome: name,
            crm: CRM,
            especialidade: specialty,
            possuiPlanoSaude: acceptPlan,
            estaAtivo: true,
            imagem: urlImage,
            senha: password,
            planosSaude: selectPlans,
            email: email,
            telefone: phone,
            endereco: {
                cep: code,
                rua: street,
                estado: state,
                numero: number,
                complemento: complement
            }
        }

        await createData ({url: "especialista", data: profissional, token: user.token})
    }


    return (
        <Modal 
            open={open}
            onClose={ handleClose}
            aria-labelledby= "Modal de cadastro do usuário"
            aria-describedby="Campo de preenchimento necessários para especialista"
        >
            <CustomBox>
                <Titulo>Cadastre o especialista inserindo os dados abaixo: </Titulo>
                <form onSubmit={handleSubmit}>
                    <Input
                        type="text"
                        value={name}
                        placeholder="Digite o nome da clínica"
                        required={true} 
                        onChange={setName}
                        label="Nome"
                    />

                    <Input
                        type="email"
                        value={email}
                        placeholder="Insira sua senha"
                        required={true} 
                        onChange={setEmail}
                        label="E-mail"
                    />

                    <Input
                        type="password"
                        value={password}
                        placeholder="Digite sua senha"
                        required={true} 
                        onChange={setPassword}
                        label="Crie uma senha"
                    />

                    <Input
                        type="password"
                        value={validatedPassword}
                        placeholder="Repita a senha anterior"
                        required={true} 
                        onChange={setValidatedPassword}
                        label="Repita a senha"
                    />

                    <Input
                        type="text"
                        value={specialty}
                        placeholder="Qual sua especialide? "
                        required={true} 
                        onChange={setSpecialty}
                        label="Especialide"
                    />

                    <Input
                        type="text"
                        value={CRM}
                        placeholder="Insira seu número de registro"
                        required={true} 
                        onChange={setCRM}
                        label="CRM"
                    />

                    <Input
                        type="text"
                        value={phone}
                        placeholder="(DDD) XXXXX-XXXX"
                        required={true} 
                        onChange={setPhone}
                        label="Telefone"
                    />

                    <Input
                        type="text"
                        value={urlImage}
                        placeholder="https://img.com/fotos/user-image.png"
                        required={true} 
                        onChange={setUrlImage}
                        label="Insira a URL da imagem"
                    />
                   
                    <Text><br/>Endereço<br/></Text>
                    <Input
                        type="text"
                        value={code}
                        placeholder="Insira o CEP"
                        required={true} 
                        onChange={setCode}
                        label="CEP"
                        icon="fa-sharp fa-solid fa-location-dot"
                    />
                   <ContainerGrid>
                        <Input
                            type="text"
                            value={street}
                            placeholder="Rua"
                            required={true} 
                            onChange={setStreet}
                        />

                        <Input
                            type="text"
                            value={number}
                            placeholder="Número"
                            required={true} 
                            onChange={setNumber}
                        />

                        <Input
                            type="text"
                            value={complement}
                            placeholder="Complemento"
                            required={true} 
                            onChange={setComplement}
                        />

                        <Input
                            type="text"
                            value={state}
                            placeholder="Estado"
                            required={true} 
                            onChange={setState}
                        />
                    </ContainerGrid>

                    <ContainerGridCenter>
                        <Text><br/>Atende por plano?<br/></Text>
                        <SwitchPlan
                            aria-label="Sim / Não"
                            checked={acceptPlan}
                            onChange={() => {
                                acceptPlan ?
                                    setAcceptPlan(false) :
                                    setAcceptPlan(true)
                            }}
                        />
                        <TextSwitch>Não/Sim</TextSwitch>
                    </ContainerGridCenter>

                    {acceptPlan ?
                        <>
                            <Text><br/>Selecione os planos<br/></Text>
                            <FormGroup>
                                <FormControlLabel control={<Checkbox onChange={handleChangePlans} value={1} />} label="Sulamerica" />
                                <FormControlLabel control={<Checkbox onChange={handleChangePlans} value={2} />} label="Unimed" />
                                <FormControlLabel control={<Checkbox onChange={handleChangePlans} value={3} />} label="Bradesco" />
                                <FormControlLabel control={<Checkbox onChange={handleChangePlans} value={4} />} label="Amil" />
                                <FormControlLabel control={<Checkbox onChange={handleChangePlans} value={5} />} label="Biosaúde" />
                                <FormControlLabel control={<Checkbox onChange={handleChangePlans} value={6} />} label="Biovida" />
                                <FormControlLabel control={<Checkbox onChange={handleChangePlans} value={7} />} label="Outro" />
                            </FormGroup>
                        </>
                    : ''}

                    <ButtonCustom type="submit">Avançar</ButtonCustom>
                </form>
            </CustomBox>
        </Modal>
    )
}