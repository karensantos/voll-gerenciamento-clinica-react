import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import styled from "styled-components";
import Input from "../../components/Input";
import Button from "../../components/Botao"
import usePost from "../../usePost";
import authStore from "../../stores/auth.store";


const TitlePage = styled.h2`
font-weight: 700;
font-size: 24px;
line-height: 28px;
color: var(--cinza)
`;

const WrapperForm = styled.form`
width: 70%;
display: flex;
flex-direction: column;
align-items: center;
`;

const Text = styled.p`
font-weight: 400;
font-size: 16px;
line-height: 19px;
color: var(--azul-escuro)
`;

const TextSecondary = styled(Text)`
color: var(--cinza);
`;

const LinkCustom = styled(Link)`
color: var(--azul-claro);
font-weight: 700;
text-decoration: none;
`;

const ButtonCustom = styled(Button)`
  width: 50%;
`;

interface ILogin {
    email: string,
    senha: string
}

export default function Login() {
    const [email, setEmail] =  useState('');
    const [password, setPassword] =  useState('');
    const {createData, success, error, response } = usePost();
    const navigate = useNavigate();

    const handleLogin = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const user: ILogin = {
            email: email,
            senha: password
        }

        try {
            createData({url:'auth/login', data: user})
            authStore.login({email: email, token: response})
            response && navigate('/dashboard');
        } catch(error) {
            error && alert ('Não foi possível realziar login')
        }
    }

    return (
        <>
            <TitlePage>Faça login em sua conta</TitlePage>
            <WrapperForm onSubmit={handleLogin}>
                <Input
                type="text"
                value={email}
                placeholder="Insira seu endereço de e-mail"
                required={true} 
                onChange={setEmail}
                label="E-mail"
                />

                <Input
                type="password"
                value={password}
                placeholder="Insira sua senha"
                required={true} 
                onChange={setPassword}
                label="Senha"
                />
                <ButtonCustom type="submit">Entrar</ButtonCustom>
            </WrapperForm>

            <Text>Esqueceu sua senha?</Text>
            <TextSecondary>Ainda não tem conta? <LinkCustom to="/cadastro">Faça seu cadastro!</LinkCustom></TextSecondary>

        </>
    )
}