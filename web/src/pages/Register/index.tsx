import {Stepper, Step, StepLabel} from "@mui/material"
import { useState } from "react"
import styled from "styled-components"
import Input from "../../components/Input";
import Button from "../../components/Botao"
import IClinica from "../../types/IClinica";
import usePost from "../../usePost";
import { useNavigate } from "react-router-dom";

interface PropsCustom {
    color: string
}

const StepCustom = styled.div<PropsCustom>`
background-color: ${({color})=> color};
width: 16px;
height: 16px;
border-radius: 50%;
`;

const TitlePage = styled.h2`
font-weight: 700;
font-size: 24px;
line-height: 28px;
color: var(--cinza);
text-align: center;
`;

const WrapperForm = styled.form`
width: 70%;
display: flex;
flex-direction: column;
align-items: center;
`;

const ButtonCustom = styled(Button)`
width: 50%;
`;

const Text = styled.p`
font-weight: 400;
font-size: 16px;
line-height: 19px;
color: var(--azul-escuro);
text-align: left;
width: 100%;
font-weight: 700;
`;

const ContainerGrid = styled.div`
display: grid;
width: 100%;
grid-template-columns: 30% 65%;
justify-content: space-between;
`;

export default function Register() {

    const [stageActive, setStageActive] = useState(0)
    const [name, setName] = useState('');
    const [cnpj, setCNPJ] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [validatedPassword, setValidatedPassword] = useState('');
    const [phone, setPhone] = useState('');
    const [code, setCode] = useState('');
    const [street, setStreet] = useState('');
    const [number, setNumber] = useState('');
    const [complement, setComplement] = useState('');
    const [state, setState] = useState('');
    const {createData, error, success} = usePost();
    const navigate = useNavigate();

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const clinica: IClinica = {
            nome: name,
            cnpj: cnpj,
            email: email,
            senha: password,
            endereco: {
                cep: code,
                rua: street,
                numero: number,
                complemento: complement,
                estado: state
            }
        }

        if(stageActive !== 0) {
            try{ 
                createData({url: 'clinica', data: clinica});
                navigate('/login')
            } catch(error) {
                error && alert('Erro ao cadastrar os dados')
            }
        }

        setStageActive(stageActive + 1)
    }

    return (
        <>
            <Stepper activeStep={stageActive}>
                <Step>
                    <StepLabel
                        StepIconComponent={(props) => (
                            <StepCustom color={ props.active ? 'lightblue' : 'lightgray'}/>
                        )}
                    />
                </Step>

                <Step>
                    <StepLabel
                        StepIconComponent={(props) => (
                            <StepCustom color={ props.active ? 'lightblue' : 'lightgray'}/>
                        )}
                    />
                </Step>
            </Stepper>

            {stageActive === 0 ?

                (<>
                    <TitlePage>Primeiro, alguns dados <br/> básicos:</TitlePage>
                    <WrapperForm onSubmit={handleSubmit}>
                        <Input
                            type="text"
                            value={name}
                            placeholder="Digite o nome da clínica"
                            required={true} 
                            onChange={setName}
                            label="Nome"
                        />
                        <Input
                            type="text"
                            value={cnpj}
                            placeholder="Digite o CNPJ"
                            required={true} 
                            onChange={setCNPJ}
                            label="CNPJ"
                        />

                        <Input
                            type="email"
                            value={email}
                            placeholder="Insira sua senha"
                            required={true} 
                            onChange={setEmail}
                            label="E-mail"
                        />

                        <Input
                            type="password"
                            value={password}
                            placeholder="Digite sua senha"
                            required={true} 
                            onChange={setPassword}
                            label="Crie uma senha"
                        />

                        <Input
                            type="password"
                            value={validatedPassword}
                            placeholder="Repita a senha anterior"
                            required={true} 
                            onChange={setValidatedPassword}
                            label="Repita a senha"
                        />
                        <ButtonCustom type="submit">Avançar</ButtonCustom>
                    </WrapperForm>
                </>)
                :
                (<>
                    <TitlePage>Agora, os dados técnicos:</TitlePage>
                    <WrapperForm onSubmit={handleSubmit}>
                    <Input
                        type="text"
                        value={phone}
                        placeholder="(DDD) XXXXX-XXXX"
                        required={true} 
                        onChange={setPhone}
                        label="Telefone"
                    />

                    <Text><br/>Endereço<br/></Text>
                    <Input
                        type="text"
                        value={code}
                        placeholder="Insira o CEP"
                        required={true} 
                        onChange={setCode}
                        label="CEP"
                        icon="fa-sharp fa-solid fa-location-dot"
                    />

                    <Input
                        type="text"
                        value={street}
                        placeholder="Rua"
                        required={true} 
                        onChange={setStreet}
                    />

                    <ContainerGrid>
                        <Input
                            type="text"
                            value={number}
                            placeholder="Número"
                            required={true} 
                            onChange={setNumber}
                        />

                        <Input
                            type="text"
                            value={complement}
                            placeholder="Complemento"
                            required={true} 
                            onChange={setComplement}
                        />

                        <Input
                            type="text"
                            value={state}
                            placeholder="Estado"
                            required={true} 
                            onChange={setState}
                        />

                    </ContainerGrid>
                    
                    <ButtonCustom type="submit">Cadastrar</ButtonCustom>

                </WrapperForm>
                </>)
            }
        </>
    )
}