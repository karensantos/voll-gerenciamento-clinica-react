import styled from "styled-components"

const Container = styled.div`
width: 100%
`
const LabelElement = styled.label`
display: block;
font-weight: 700;
font-size: 16px;
line-height: 19px;
color: var(--azul-escuro);
`;

const InputElement = styled.input`
background: #F8F8F8;
box-sizing: border-box;
border: none;
width: 100%;
padding: 16px;
`;

const InputGroup = styled.div`
border-radiu: 8px;
background: #F8F8F8;
display: flex;
justify-content: center;
align-items: center;
width: 100%;
margin: 1em 0;
box-shadow: 2px 2px 6px rgba(0,0,0, 0.25);
`;

const Icon = styled.i`
background-color: #EDEDED;
height: 52px;
width: 56px;
display: flex;
align-items: center;
justify-content: center;
`;

interface Props {
    type: string, 
    value: string, 
    placeholder: string, 
    required?: boolean,
    label?: string,
    icon?: string,
    onChange: (value: string) => void; 
}

export default function Input({type, value, placeholder, onChange, required, label, icon} : Props) {
    return(
        <Container>
            <LabelElement htmlFor="">{label}</LabelElement>

            <InputGroup className="input-group">
                {icon ? <Icon className={icon} /> : ''}
                <InputElement 
                    type={type}
                    value={value}
                    placeholder={placeholder}
                    onChange={(e)=> onChange(e.target.value)}
                    required={required ? true: false }
                />
            </InputGroup>
        </Container>
    )
}