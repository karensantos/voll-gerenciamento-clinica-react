import { Link } from 'react-router-dom';
import styled from 'styled-components';
import authStore from '../../stores/auth.store';
import logo from './assets/logo.png';
import perfil from './assets/perfil.png';

const CabecalhoEstilizado = styled.header`
    display:flex;
    align-items: center;
    justify-content: space-between;
    padding: 2em 4em
`

const Container = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-around;
    gap: 16px;
`

const ContainerLinks = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-around;
    gap: 16px;
`

const ContainerSearch = styled.div`
    display: flex;
    align-items: center;
    gap: 16px;
    border: 1px solid #90989F;
    border-radius: 8px;
    padding: 0 14px;
`
const Button= styled(Link)`
    background-color: var(--azul-escuro);
    border-radius: 8px;
    padding: 12px 16px;
    color: var(--branco);
    border: none;
    font-weight: 700;
    line-height: 19px;
    text-decoration: none;
`
const ButtonSubmit = styled.button`
color: var(--azul-escuro);
font-weight: 700;
border: none;
background: transparent;
font-size: 16px;
text-decoration: underline;
margin: 0 8px;
cursor: pointer;
`

const LinkCustom = styled(Link)`
color: var(--azul-escuro);
font-weight: 400;
text-decoration: none;
font-size: 16px;
line-height: 19px;
text-align: center;
`;

const LinkCustomLogout = styled.a`
font-weight: 700;
`

const InputSearch = styled.input`
border: none;
font-size: 16px;
padding: 16px 4px;
`

const ButtonSearch = styled(ButtonSubmit)`
margin: 0;
`

function Cabecalho() {
   
    const handleLogout = ()=>{
       authStore.logout();
    }
    
    return (
        <CabecalhoEstilizado>
            <img src={logo} alt="logo da empresa Voll" />

            {authStore.authenticated ?
                <Container>
                    <img src={perfil} alt="imagem de perfil do usuário" />
                    <LinkCustomLogout href="/" onClick={handleLogout}>Sair</LinkCustomLogout>
                </Container>
                :
                <Container>
                    <ContainerLinks>
                        <LinkCustom to="/about">Sobre</LinkCustom>
                        <LinkCustom to="/register">Cadastre-se</LinkCustom>
                    </ContainerLinks>
                    <ContainerSearch>
                        <InputSearch type="text" placeholder="Digite sua busca"/>
                        <ButtonSearch type="submit">
                            <i className="fa-solid fa-magnifying-glass"></i>
                        </ButtonSearch>
                    </ContainerSearch>
                    <Button to="/login">Login</Button>
                </Container>
            }
        </CabecalhoEstilizado>
    )
}

export default Cabecalho;