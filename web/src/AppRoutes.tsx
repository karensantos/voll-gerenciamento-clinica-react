import { BrowserRouter, Route, Routes } from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import TemplateBase from "./templates/TemplateBase";
import TemplateForm from "./templates/TemplateForm";
import PrivateRoute from "./utils/PrivateRoute";

function AppRoutes() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<TemplateBase/>}> 
                    <Route index element={<Home/>} />  
                    <Route element={<PrivateRoute />}>
                        <Route path="/dashboard" element={<Dashboard/>} />    
                    </Route>
                </Route>

                <Route path="/" element={<TemplateForm/>}> 
                    <Route path="/login" element={<Login/>} />    
                    <Route path="/register" element={<Register/>} />    
                </Route>
            </Routes>
        </BrowserRouter>
    )
}

export default AppRoutes